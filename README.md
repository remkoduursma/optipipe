OptiPipe : an R implementation of the model of Valentine and Makela (2012)
============================================

Valentine, H.T., Mäkelä, A., 2012. Modeling forest stand dynamics from optimal balances of carbon and nitrogen. New Phytologist 194, 961–971. doi:10.1111/j.1469-8137.2012.04123.x


To install this R package, use the `devtools` package. The `OptiPipe` function contains a useful example.

```
library(devtools)
install_bitbucket("remkoduursma/optipipe")
library(OptiPipe)
?OptiPipe
```